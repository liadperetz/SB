﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Connections;
using System.IO;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Scan.xaml
    /// </summary>
    public partial class Scan : Page
    {
        //Sets a timer
        //Used for: 1. every tick the "Loading..." animation changes
        //          2. Time out for the client (connection)
        DispatcherTimer timer = new DispatcherTimer();
        DateTime endDate = new DateTime();
        TimeSpan timeToGo = new TimeSpan(0, 5, 5);

        //INIT
        private static string _filepath = @"C:\Users\User\Desktop\SB\reports\";
        static string publicScannedFilePath = "";

        private System.Object loadingLableLock = new System.Object();
        private static System.Object boolLock = new System.Object();

        private static Connection serverConnection;
        private static bool isReportRecieved = false;
        private static Thread clientThread = null;

        public Scan (Connection conn, string scannedFilePath)
        {
            //INIT
            isReportRecieved = false;
            serverConnection = conn;
            InitializeComponent();
            changeLoadingLable();
            publicScannedFilePath = scannedFilePath;

            this.timer.Tick += new EventHandler(timer_Tick);
            this.timer.Interval = new TimeSpan(0, 0, 1);

            this.endDate = DateTime.Now.Add(timeToGo);

            //Starts the timer
            this.timer.Start();

            //Starts a thread for the client (connection)
            clientThread = new Thread(new ThreadStart(GetReport));
            clientThread.Start();
        }

        private static void GetReport()
        {
            string filename = "";
            Dictionary<object, object> input = (Dictionary<object, object>)serverConnection.Read();

            //serverConnection.SendObject("Success");

            //Create the file path
            filename = System.IO.Path.Combine(_filepath, ((string)input["filename"]));

            //Write it to a .exe file
            WriteToFile((byte[])input["Content"], filename);
            
            //Used to notify that the file has recieved
            lock (boolLock)
            {
                isReportRecieved = true;
            }
        }

        /*Input: content - the content of the .exe file that the user sent to check
         *Output: None.
         *Description: The function gets the file content from the user and create the same file in the VM.*/
        private static void WriteToFile(byte[] content, string filename)
        {
            //If is there already a file with this name in this directory, it will delete it.
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }

            /*using (FileStream file = File.Create(_filename)) //_filename = Path + filename.
            {
                file.Write(content, 0, content.Length);
            }*/

            FileStream fileStream = File.Create(filename);
            fileStream.Close();

            using (FileStream output = new FileStream(filename, FileMode.Open, FileAccess.Write))
            {
                output.Write(content, 0, content.Length);
                //CHECK
                Console.WriteLine("\nI finished copy\n");
            }
        }

        //The tick of the timer, changes the "Loading.."
        void timer_Tick(object sender, EventArgs e)
        {
            changeLoadingLable();

            //Checks if report recieved
            lock (boolLock)
            {
                if (isReportRecieved == true)
                {
                    try
                    {
                        clientThread.Abort();
                        this.timer.Stop();
                        NavigationService.Navigate(new SingleLogView(publicScannedFilePath));
                    }
                    catch (Exception ex) { }
                }
            }
              
            //If the timer has finished
            if (this.endDate <= DateTime.Now)
            {
                this.timer.Stop();
                //NavigationService.Navigate(new SingleLogView());

                lock (boolLock)
                {
                    if(isReportRecieved == true)
                    {
                        try
                        {
                            NavigationService.Navigate(new SingleLogView(publicScannedFilePath));
                        }
                        catch (Exception ex) { }
                    }
                }
            }
        }

        //Changes the loading lable by the time
        public void changeLoadingLable()
        {
            lock (loadingLableLock)
            {
                switch (DateTime.Now.Second % 3)
                {
                    case 0:
                        scanningLable.Content = "Scanning.";
                        break;
                    case 1:
                        scanningLable.Content = "Scanning..";
                        break;
                    case 2:
                        scanningLable.Content = "Scanning...";
                        break;
                }
            }
        }
    }
}
