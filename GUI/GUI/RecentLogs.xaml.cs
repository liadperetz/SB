﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI
{
    /// <summary>
    /// Interaction logic for RecentLogs.xaml
    /// </summary>
    public partial class RecentLogs : Page
    {
        public RecentLogs()
        {
            InitializeComponent();
            resetExpenders();
        }

        //Creates the expanders, each for a log
        //Used to INIT the page or to do a refresh
        private void resetExpenders()
        {
            //Resets the expandersStackPanel
            ExpendersStackPanel.Children.Clear();

            //Reads the recent logs file
            //string allLogsPath = System.IO.Directory.GetCurrentDirectory() + @"\RecentLogs.txt";
            string path = @"C:\Users\User\Desktop\SB\Recent Logs\RecentLogs.txt";
            string[] lines = System.IO.File.ReadAllLines(path);

            List<int> linesOfFileName = getLinesOfFileName(lines);

            //If there is at least 1 log
            if(linesOfFileName.Count != 0)
            {
                Expander expander;
                Button button;
                DockPanel dockPanel;

                for (int i = 0; i < linesOfFileName.Count; i++)
                {
                    dockPanel = new DockPanel();
                    
                    //Gets the expander from the suitable function
                    expander = getExpanderFromIndex(lines, linesOfFileName[i]);

                    //Gets the button from the suitable function
                    button = getButton(i);

                    //Adds them to the dockpanel
                    dockPanel.Children.Add(button);
                    dockPanel.Children.Add(expander);
                    dockPanel.Name = "dockpanel" + i.ToString();
                    
                    //Adds the dockPanel to the main stackPanel
                    ExpendersStackPanel.Children.Add(dockPanel);
                }
            }
        }

        //Creates and returnes a specefic expander
        private Expander getExpanderFromIndex(string[] lines, int nameIndex)
        {
            Expander expander = new Expander();
            string value = "";

            //Gets the file name as "value"
            string[] stringSeperators = new string[] { "File Name: " };
            value = lines[nameIndex].Split(stringSeperators, StringSplitOptions.None)[1];

            //Adds the file name to the current expander
            expander.Header = "\"" + value + "\" Scan Log";
            expander.Style = this.FindResource("ExpenderStyle") as Style;

            StackPanel labelsStackPanel = new StackPanel();

            //Sets the margin for the stack panel
            Thickness margin = new Thickness();
            margin.Left = 10;
            margin.Top = 4;
            labelsStackPanel.Margin = margin;

            //Adds the labels of the expander
            Label label;

            int i = 0;
            while (true)
            {
                if(lines[nameIndex + 2 + i] != "###")
                {
                    //Addes a new lable, sets the style by the content
                    label = new Label();
                    label.Style = this.FindResource(i < 4 ? "ExpenderLabelStyle" : "ExpenderScanResultsLabelStyle") as Style;

                    Thickness labelMargin = new Thickness();
                    labelMargin.Left = 16;
                    label.Margin = labelMargin;

                    /*Adds the content of the line to the stackPanel.
                    If the content is the value of the "Problems Found",
                    Then adds a small indentation before.*/

                    label.Content = (i < 4 ? "" : "\t       ") + lines[nameIndex + 1 + i];

                    labelsStackPanel.Children.Add(label);
                    i++;
                }
                else
                {
                    break;
                }
            }
            

            //Adds the completed stack panel to the expander
            expander.Content = labelsStackPanel;

            return expander;
        }

        //Creates and returnes a delete button, linked to a specific expander
        private Button getButton(int expanderNum)
        {
            //Creates the shape of the button
            FrameworkElementFactory ellipseFactory = new FrameworkElementFactory(typeof(Ellipse));
            ellipseFactory.SetValue(Ellipse.StrokeProperty, new SolidColorBrush(Colors.DimGray));
            ellipseFactory.SetValue(Ellipse.StrokeThicknessProperty,  0.5);
            ellipseFactory.SetValue(Ellipse.FillProperty, new SolidColorBrush(Colors.WhiteSmoke));

            //Creates content presenter which holds the buttons content
            FrameworkElementFactory contentPresenterFactory = new FrameworkElementFactory(typeof(ContentPresenter));
            contentPresenterFactory.SetValue(ContentPresenter.VerticalAlignmentProperty , VerticalAlignment.Center);
            contentPresenterFactory.SetValue(ContentPresenter.HorizontalAlignmentProperty, HorizontalAlignment.Center);

            //Creates a grid to be added in the buttons template
            FrameworkElementFactory gridFactory = new FrameworkElementFactory(typeof(Grid));
            gridFactory.AppendChild(ellipseFactory);
            gridFactory.AppendChild(contentPresenterFactory);

            FrameworkElementFactory mainPanel = new FrameworkElementFactory(typeof(DockPanel));
            //mainPanel.SetValue(DockPanel.LastChildFillProperty, true);
            mainPanel.AppendChild(gridFactory);

            //Creates the button, adds margin
            Button btn = new Button();
            DockPanel.SetDock(btn, Dock.Left);
            btn.Width = 18;
            btn.Height = 18;
            btn.Content = "X";
            btn.Foreground = new SolidColorBrush(Colors.DarkGray);
            btn.FontWeight = FontWeights.Bold;

            //Adds a unique name to the button and links the click to the click function
            btn.Name = "deleteButton" + expanderNum.ToString();
            btn.Click += new RoutedEventHandler(deleteButtonClick);

            Thickness margin = new Thickness();
            margin.Top = 9;
            btn.Margin = margin;

            //Assigns the grid(which is inside "mainPanel") FrameworkElementFactory to the buttons template
            ControlTemplate template = new ControlTemplate(typeof(Button));
            template.VisualTree = mainPanel;
            btn.Template = template;

            return btn;
        }

        //Finds the line where the file name is written in the logs list
        private List<int> getLinesOfFileName(string[] lines)
        {
            List<int> linesOfFileName = new List<int>();
            int i = 0;

            //For each line, checks if it hold a file name (the beggining of the log)
            for (i = 0; i < lines.Length; i++)
            {
                if (lines[i] == (string)("-Log#" + (linesOfFileName.Count + 1).ToString() + "-"))
                {
                    linesOfFileName.Add(i + 1);
                }
            }

            return linesOfFileName;
        }

        //Go back Button
        private void Button_Click_Back(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new MainPage());
        }

        //Delete a log Button
        private void deleteButtonClick(object sender, RoutedEventArgs e)
        {
            //Gets the button name from the sender object
            Button btn = (Button)sender;
            string btnName = btn.Name;
            int i = 0, btnNum = 0;

            //Gets the button num from the btnName,
            //Starting from the 12'th index because the
            //first 12 chars are "deleteButton"
            for (i = 12; i < btnName.Length; i++)
            {
                btnNum *= 10;
                btnNum += btnName[i] - '0';
            }

            //Deletes the suitble log
            deleteLogFromFile(btnNum, ExpendersStackPanel.Children.Count + 1);
            resetExpenders();
        }

        //Deletes a given log from the logs.txt file
        private void deleteLogFromFile(int logNumber, int numberOfLogs)
        {
            //string allLogsPath = System.IO.Directory.GetCurrentDirectory() + @"\RecentLogs.txt";
            string path = @"C:\Users\User\Desktop\SB\Recent Logs" + @"\RecentLogs.txt";
            string[] lines = System.IO.File.ReadAllLines(path);
            int i = 0;

            //Creates a list of all of the lines
            List<string> linesList = new List<string>(lines);

            int fileNameIndex = getLinesOfFileName(lines)[logNumber] - 1;

            //Removes all of the log's data, until reaches ###
            while (true)
            {
                if(linesList[fileNameIndex] == "###")
                {
                    break;
                }

                linesList.RemoveAt(fileNameIndex);
            }

            //Removes the 2 following lines ("###", and empty line)
            linesList.RemoveAt(fileNameIndex);
            linesList.RemoveAt(fileNameIndex);

            //Writes the lines back
            File.WriteAllLines(path, linesList);

            //Now fixes the log's numbers that has been messed
            string fileText = File.ReadAllText(path);

            for (i = logNumber; i < numberOfLogs; i++)
            {
                string replaceThis = "-Log#" + (i + 2).ToString() + "-";
                string withThat = "-Log#" + (i + 1).ToString() + "-";

                fileText = fileText.Replace(replaceThis, withThat);
                File.WriteAllText(path, fileText);
            }
        }

        //Open log.txt Button
        private void openLogsInTxt_Click(object sender, RoutedEventArgs e)
        {
            string path = @"C:\Users\User\Desktop\SB\Recent Logs\RecentLogs.txt";
            System.Diagnostics.Process.Start(path);
        }
    }
}