﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI
{
    /// <summary>
    /// Interaction logic for SingleLogView.xaml
    /// </summary>
    public partial class SingleLogView : Page
    {

        string logPath = @"C:\Users\User\Desktop\SB\reports\report.txt";             //LOG PATH
        static string publicScannedFilePath;

        public SingleLogView(string scannedFilePath)
        {
            //INIT
            InitializeComponent();
            showLog();
            addLogToList();
            publicScannedFilePath = scannedFilePath;
        }

        //Shows the recent recieved log
        private void showLog()
        {
            //Reads the recent recieved log
            string path = System.IO.Directory.GetCurrentDirectory() + logPath;
            
            string[] lines = System.IO.File.ReadAllLines(logPath);
            
            //Adds the lables to the stack panel
            Label label;

            //Adds the dockPanel to the main stackPanel
            for (int i = 0; i < lines.Length; i++)
            {
                label = new Label();
                label.Style = this.FindResource("LabelStyle") as Style;
                label.Content = (i < 5 ? "" : "\t      ") + lines[i];
                
                singleLogSackPanel.Children.Add(label);
            }    
        }

        private void addLogToList()
        {
            //Gets all of the lines from the last lof file
            string[] lastLogLines = System.IO.File.ReadAllLines(logPath);

            //Gets the lines of all the logs
            string allLogsPath = @"C:\Users\User\Desktop\SB\Recent Logs\RecentLogs.txt";
            string[] allLogsLines = System.IO.File.ReadAllLines(allLogsPath);

            //Casting the array into lists
            List<int> linesOfFileName = getLinesOfFileName(allLogsLines);
            int lastLogNum = linesOfFileName.Count + 1;
            List<string> allLogsList = new List<string>(allLogsLines);

            allLogsList.Add("-Log#" + lastLogNum + "-");

            //Adds the last log to the list
            foreach (string line in lastLogLines)
            {
                allLogsList.Add(line);
            }

            //End of log.txt file
            allLogsList.Add("");
            allLogsList.Add("###");
            allLogsList.Add("");

            //Writes all of the logs back
            System.IO.File.WriteAllLines(allLogsPath, allLogsList);
        }

        //Finds the line where the file name is written in the logs list
        //Recieves an array containing the files of the logs.txt file
        //Returnes a list with the number of line where each report starts
        private List<int> getLinesOfFileName(string[] lines)
        {
            List<int> linesOfFileName = new List<int>();
            int i = 0;

            for (i = 0; i < lines.Length; i++)
            {
                if (lines[i] == (string)("-Log#" + (linesOfFileName.Count + 1).ToString() + "-"))
                {
                    linesOfFileName.Add(i + 1);
                }
            }

            return linesOfFileName;
        }

        //Back Button
        private void Button_Click_Back(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new MainPage());
        }

        //Recent logs Button
        private void viewRecentLogs_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new RecentLogs());
        }

        //Delete file Button
        private void deleteFile_Click(object sender, RoutedEventArgs e)
        {
            File.Delete(publicScannedFilePath);
            deleteFileButton.Visibility = Visibility.Hidden;
        }
    }
}
