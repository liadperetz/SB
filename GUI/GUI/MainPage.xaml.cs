﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using Connections;

namespace GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        //INIT Global variables
        private static string _filename = @"C:\Users\User\Desktop\SB\check.exe";
        private static string _ipAddress = "192.168.1.14";
        private static Connection serverConnection;

        public MainPage()
        {
            InitializeComponent();
        }

        static int sendToServer(string path)
        {
            _filename = path;

            //Create TcpClient for create Connection variable.
            IPEndPoint serverIpEndPoint = new IPEndPoint(GetIpAddress(), 8095);
            TcpClient server = new TcpClient();
            server.Connect(serverIpEndPoint);

            //Craete Connection variable
            serverConnection = new Connection(server);

            //Console.WriteLine("Created connection\n");

            if (GetResponse(serverConnection) == true)
            {
                SendFile(serverConnection);
            }

            //Get the response from the server - The file transfered well or not
            string response = (string)serverConnection.Read();
            //Console.WriteLine("Transformation: {0}", response);
            return 1;
        }

        private static void ResponseMessage(Connection connect)
        {
            connect.SendObject("Success");
        }



        /*Input: content - the content of the .exe file that the user sent to check
         *Output: None.
         *Description: The function gets the file content from the user and create the same file in the VM.*/
        private static void WriteToFile(byte[] content, string filename)
        {
            //If is there already a file with this name in this directory, it will delete it.
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }

            /*using (FileStream file = File.Create(_filename)) //_filename = Path + filename.
            {
                file.Write(content, 0, content.Length);
            }*/

            FileStream fileStream = File.Create(filename);
            fileStream.Close();

            using (FileStream output = new FileStream(filename, FileMode.Open, FileAccess.Write))
            {
                output.Write(content, 0, content.Length);
                //CHECK
                //Console.WriteLine("\nI finished copy\n");
            }
        }

        private static void SendFile(Connection serverConnection)
        {
            //INIT
            byte[] content = ReadFile();
            Dictionary<object, object> messageContent = new Dictionary<object, object>();

            //Enter values to the dictionary.
            messageContent["filename"] = FindFilename();
            messageContent["exeContent"] = ReadFile();

            //Send the file
            Connection.JsonToString(messageContent);

            serverConnection.SendObjectBinary(messageContent);

            //Console.WriteLine("Sent the file :D");

        }

        /*Input: None
        *Output: ip - String include the IP address the user entered.
        *Description: The function receive Ip address from the user, checks if it legall and then return it.*/
        private static byte[] ReadFile()
        {
            //Read the exe file to an array of byte 
            byte[] buffer = File.ReadAllBytes(_filename);

            return buffer;
        }

        private static bool GetResponse(Connection connect)
        {
            if ((string)connect.Read() == "Success")
            {
                return true;
            }
            return false;
        }

        /*Input: None
         *Output: ip - String include the IP address the user entered.
         *Description: The function receive Ip address from the user, checks if it legall and then return it.*/
        private static IPAddress GetIpAddress()
        {
            //INIT
            string ips = null;
            IPAddress ip = null;
            bool isValid = false;
            //Run until the user enter legal IP address
            while (!isValid)
            {
                //Gets the IP address from the user.
                Console.WriteLine("Enter the IP address of the server: ");

                ips = _ipAddress;
                //ips = Console.ReadLine();
                
                //Check if the IP address is legal.
                isValid = IPAddress.TryParse(ips, out ip);
            }

            //CHECK
            Console.WriteLine("IP: {0}", ip);

            return ip;
        }

        private static string FindFilename()
        {
            //string output = 
            return _filename.Substring(_filename.LastIndexOf('\\') + 1, (_filename.Length - _filename.LastIndexOf('\\') - 1));
            //output = output.Substring(1, output.Length);
            //return output;
        }

        private void Button_Click_Scan_File(object sender, RoutedEventArgs e)
        {
            //RETURN THE PROCCESS

            //Process.Start(@"C:\Users\User\Desktop\Sandy\send file  Client Server\HTTPServer\HTTPServer\bin\Debug\HTTPServer.exe");

            MessageBox.Show("The file was transfered succefully");

            //MessageBox.Show("Error! The file was Not transfered succefully");

            int response = sendToServer(txtEditor.Text);
            NavigationService.Navigate(new Scan(serverConnection, txtEditor.Text));
        }

        //Recent logs button
        private void Button_Click_recent_logs(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new RecentLogs());
        }

        //Open File button
        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            //Opens the file explorer
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "exe Files (*.exe)|*.exe|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = @"C:\Users\User\Desktop\";

            //If a file was chosen
            if (openFileDialog.ShowDialog() == true)
            {
                txtEditor.Text = openFileDialog.FileName;
                txtEditor.Visibility = Visibility.Visible;
                analysisButton.Visibility = Visibility.Visible;
                selectedFilesLabel.Visibility = Visibility.Visible;
            }
        }

        //Drag pannel - drop action
        private void ImagePanel_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                //Sets the txt to include the path, and sets the suitible objects visible
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                txtEditor.Text = files[0];
                txtEditor.Visibility = Visibility.Visible;
                analysisButton.Visibility = Visibility.Visible;
                selectedFilesLabel.Visibility = Visibility.Visible;
            }
        }
    }
}
