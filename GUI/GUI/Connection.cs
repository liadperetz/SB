﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Connections
{
	public class Connection
	{
		private TcpClient sock;

		public IPAddress Address => (sock.Client.RemoteEndPoint as IPEndPoint).Address;
		public int Port => (sock.Client.RemoteEndPoint as IPEndPoint).Port;

		public Connection(TcpClient sock)
		{
			this.sock = sock;
		}

		~Connection()
		{
			sock?.Close();
		}

		public void Close()
		{
			sock.Close();
		}

		public void Send(string s)
		{
			(new StreamWriter(sock.GetStream()) { AutoFlush = true, NewLine = "\n" }).Write(s);
		}

		public void SendLine(string line)
		{
			(new StreamWriter(sock.GetStream()) { AutoFlush = true, NewLine = "\n" }).WriteLine(line);
		}

		public void SendObject(object o)
		{
			(new StreamWriter(sock.GetStream()) { AutoFlush = true, NewLine = "\n" }).Write(JsonToString(o));
		}

		#region Json to string - Object to string representation
		private static string ValueToString(object val, int tabs = 0)
		{
			switch (val)
			{
				case true:
					return new string('\t', tabs) + "T";

				case false:
					return new string('\t', tabs) + "F";

				case null:
					return new string('\t', tabs) + "0";

				//number
				case int i:
					return NumberToString(i, tabs);
				case double d:
					return NumberToString(d, tabs);

				case string s:
					return StringToString(s, tabs);

				case byte[] b:
					return BinaryToString(b, tabs);

				case List<object> l:
					return ArrayToString(l, tabs);

				case Dictionary<object, object> o:
					return ObjectToString(o, tabs);

				default:
					throw new NotSupportedException("This type is not supported.");
			}
		}

		private static string NumberToString(object n, int tabs = 0)
		{
			return new string('\t', tabs) + "#" + n + "#";
		}

		private static string StringToString(string s, int tabs = 0)
		{
			s = s.Replace("\\", "\\\\");
			if (s.Contains("'") && !s.Contains("\""))
			{
				return new string('\t', tabs) + "\"" + s + "\"";
			}
			else if (s.Contains("\"") && !s.Contains("'"))
			{
				return new string('\t', tabs) + "'" + s + "'";
			}
			else
			{
				return new string('\t', tabs) + "\"" + s.Replace("\"", "\\\"") + "\"";
			}
		}

		private static string BinaryToString(byte[] b, int tabs = 0)
		{
			return new string('\t', tabs) + "b" + b.Length + "b" + Encoding.ASCII.GetString(b);
		}

		private static string ArrayToString(List<object> l, int tabs = 0)
		{
			string s = new string('\t', tabs) + "[";

			foreach (object o in l)
			{
				s += " " + ValueToString(o, tabs).Trim() + ",";
			}
			if (s[s.Length - 1] == ',')
			{
				s = s.Substring(0, s.Length - 1);
			}

			return s + " ]";
		}

		private static string ObjectToString(Dictionary<object, object> o, int tabs = 0)
		{
			string s = new string('\t', tabs) + "{";

			foreach (var field in o)
			{
				s += "\n" + ValueToString(field.Key, tabs + 1);
				if (!(field.Value is null))
				{
					s += " : " + ValueToString(field.Value, tabs + 1).TrimStart();
				}
				s += ",";
			}
			if (s[s.Length - 1] == ',')
			{
				s = s.Substring(0, s.Length - 1);
			}

			s += "\n" + new string('\t', tabs) + "}";
			return s;
		}

		private static string TabJson(string json)
		{
			int tabs = 0;
			string newJson = "";

			for (int i = 0; i < json.Length; i++)
			{
				newJson += json[i];
				switch (json[i])
				{
					case '{':
						tabs++;
						break;
					case '}':
						tabs--;
						break;
					case '\n':
						try
						{
							newJson += new string('\t', tabs);
						}
						catch (Exception) { }
						break;
				}
			}
			return newJson;
		}

		public static string JsonToString(object o)
		{
			return ValueToString(o);
		}
		#endregion

		public void SendObjectBinary(object o)
		{
			SendJson(o);
		}

		#region Send Json
		private void SendJson(object o)
		{
			SendValue(o);
		}

		private void SendValue(object o)
		{
			{
				switch (o)
				{
					case true:
						Send("T");
						return;

					case false:
						Send("F");
						return;

					case null:
						Send("0");
						return;

					//number
					case int i:
						SendNumber(i);
						return;
					case double d:
						SendNumber(d);
						return;

					case string s:
						SendString(s);
						return;

					case byte[] b:
						SendBinary(b);
						return;

					case List<object> l:
						SendArray(l);
						return;

					case Dictionary<object, object> obj:
						SendObjectValue(obj);
						return;

					default:
						throw new NotSupportedException("This type is not supported.");
				}
			}
		}

		private void SendNumber(int i)
		{
			Send(NumberToString(i));
		}

		private void SendNumber(double d)
		{
			Send(NumberToString(d));
		}

		private void SendString(string s)
		{
			Send(StringToString(s));
		}

		private void SendBinary(byte[] b)
		{
			byte[] arrToSend = ASCIIEncoding.ASCII.GetBytes("b" + b.Length + "b").ToList().Concat(b.ToList()).ToArray();
			sock.GetStream().Write(arrToSend, 0, arrToSend.Length);
		}

		private void SendArray(List<object> l)
		{
			Send("[");
			if (l.Count > 0)
			{
				foreach (object o in l.Take(l.Count - 1))
				{
					SendValue(o);
					Send(",");
				}
				SendValue(l[l.Count - 1]);
			}
			Send("]");
		}

		private void SendObjectValue(Dictionary<object, object> o)
		{
			Send("{");

			foreach (var field in o)
			{
				SendValue(field.Key);
				if (!(field.Value is null))
				{
					Send(":");
					SendValue(field.Value);
				}
				o.Remove(field);
				if (o.Count > 0)
				{
					Send(",");
				}
			}

			Send("}");
		}

		#endregion

		public string ReadLine()
		{
			return (new StreamReader(sock.GetStream())).ReadLine();
		}

		public object Read()
		{
			return ReadJson();
		}

		private char ReadChar()
		{
			byte[] b = new byte[1];

			if (sock.GetStream().Read(b, 0, 1) == 0)
			{
				throw new Exception("Connection terminated by the client");
			}

			return (char)b[0];
		}

		private byte[] ReadBytesComplete(int num)
		{
			byte[] bytes = new byte[num];
			int count = 0;

			while (count < num)
			{
				count += sock.GetStream().Read(bytes, count, num - count);
			}
			return bytes;
		}

		#region Read Json
		private object ReadJson()
		{
			return ReadValue(ReadChar());
		}

		private object ReadValue(char c)
		{
			switch (c)
			{
				//number
				case '#':
					return ReadNum(c);

				//string
				case '\'':
				case '"':
					return ReadString(c);

				//binary
				case 'b':
				case 'B':
					return ReadBinary(c);

				//array
				case '[':
					return ReadArray(c);

				//object
				case '{':
					return ReadObject(c);

				//true
				case 'T':
				case 't':
					return true;

				//false
				case 'F':
				case 'f':
					return false;

				//null
				case '0':
					return null;

				//whitespace
				case ',':
				case '\t':
				case '\n':
				case ' ':
					object o = ReadValue(ReadChar());
					return o;

				default:
					throw new FormatException("Input doesn't match the format.");
			}
		}

		private Dictionary<object, object> ReadObject(char c)
		{
			Dictionary<object, object> d = new Dictionary<object, object>();

			object key = null;
			object value = null;

			bool isVal = false;
			bool next = true;

			if (c != '{')
			{
				throw new FormatException("Input doesn't match the format.");
			}
			else
			{
				while ((c = ReadChar()) != '}')
				{
					if ((new List<char> { ' ', '\n', '\t' }).Contains(c))//whitespace
					{
						continue;
					}
					else if (c == ',')//next
					{
						next = true;
						isVal = false;
					}
					else if (c == ':')//val
					{
						if (!next == !isVal)//if already read a key and not a value
						{
							isVal = true;
						}
						else
						{
							throw new FormatException("input is not in the right format");
						}
					}
					else//value
					{
						if ((!next && !isVal) || (next && isVal))//if not next set and not a value or if next set and value
						{
							throw new FormatException("input is not in the right format");
						}

						object v = ReadValue(c);
						next = false;

						if (!isVal)//key
						{
							key = v;
							d[key] = null;
						}
						else if (isVal)//value
						{
							value = v;
							d[key] = value;
						}
					}
				}
			}

			return d;
		}

		private object ReadArray(char c)
		{
			if (c != '[')
			{
				throw new FormatException("Input doesn't match the format.");
			}

			List<object> arr = new List<object>();

			bool next = true;

			while ((c = ReadChar()) != ']')
			{
				if (c == ' ' || c == '\t' || c == '\n')
				{
					continue;
				}
				else if (c == ',')
				{
					next = true;
				}
				else
				{
					if (!next)
					{
						throw new FormatException("Input doesn't match the format.");
					}

					arr.Add(ReadValue(c));
					next = false;
				}
			}

			return arr;
		}

		private string ReadString(char c)
		{
			string s = "";
			char limiter = c;
			if (!(new char[] { '"', '\'' }.Contains(limiter)))
			{
				throw new FormatException("Input doesn't match the format.");
			}

			while ((c = ReadChar()) != limiter)
			{
				if (c == '\\')
				{
					c = ReadChar();
				}
				s += c;
			}

			return s;
		}

		private byte[] ReadBinary(char c)
		{
			string numString = "";
			if (!(new char[] { 'b', 'B' }.Contains(c)))
			{
				throw new FormatException("Input doesn't match the format.");
			}

			while (!(new char[] { 'b', 'B' }.Contains(c = ReadChar())))
			{
				if (new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' }.Contains(c))
				{
					numString += c;
				}
				else
				{
					throw new FormatException("Input doesn't match the format.");
				}
			}

			if (!int.TryParse(numString, out int num))
			{
				throw new FormatException("Input doesn't match the format.");
			}

			return ReadBytesComplete(num);
		}

		private object ReadNum(char c)
		{
			string numString = "";
			if (c != '#')
			{
				throw new FormatException("Input doesn't match the format.");
			}

			while ((c = ReadChar()) != '#')
			{
				if (new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-' }.Contains(c))
				{
					numString += c;
				}
				else
				{
					throw new FormatException("Input doesn't match the format.");
				}
			}

			if (int.TryParse(numString, out int iRes))
			{
				return iRes;
			}
			else if (double.TryParse(numString, out double dRes))
			{
				return dRes;
			}

			throw new FormatException("Input doesn't match the format.");
		}
		#endregion

		#region Parse Json - string representation to object
		private static object ParseValue(string input, out int bytesRead)
		{
			switch (input[0])
			{
				//number
				case '#':
					return ParseNum(input, out bytesRead);

				//string
				case '\'':
				case '"':
					return ParseString(input, out bytesRead);

				//binary
				case 'b':
				case 'B':
					return ParseBinary(input, out bytesRead);

				//array
				case '[':
					return ParseArray(input, out bytesRead);

				//object
				case '{':
					return ParseObject(input, out bytesRead);

				//true
				case 'T':
				case 't':
					bytesRead = 1;
					return true;

				//false
				case 'F':
				case 'f':
					bytesRead = 1;
					return false;

				//null
				case '0':
					bytesRead = 1;
					return null;

				//whitespace
				case ',':
				case '\t':
				case '\n':
				case ' ':
					//out of range exception
					object o = ParseValue(input.Substring(1), out bytesRead);
					bytesRead++;
					return o;

				default:
					bytesRead = 0;
					throw new FormatException("Input doesn't match the format.");
			}
		}

		private static object ParseNum(string input, out int bytesRead)
		{
			string numString = "";
			int i = 1;
			if (input[0] != '#')
			{
				throw new FormatException("Input doesn't match the format.");
			}

			while (input[i] != '#')
			{
				if (new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-' }.Contains(input[i]))
				{
					numString += input[i++];
				}
				else
				{
					throw new FormatException("Input doesn't match the format.");
				}
			}

			bytesRead = i + 1;

			if (int.TryParse(numString, out int iRes))
			{
				return iRes;
			}
			else if (double.TryParse(numString, out double dRes))
			{
				return dRes;
			}

			throw new FormatException("Input doesn't match the format.");
		}

		private static string ParseString(string input, out int bytesRead)
		{
			string s = "";
			int i = 1;
			char limiter = input[0];
			if (!(new char[] { '"', '\'' }.Contains(limiter)))
			{
				throw new FormatException("Input doesn't match the format.");
			}

			while (input[i] != limiter)
			{
				if (input[i] == '\\')
				{
					i++;
				}
				s += input[i++];
			}

			bytesRead = i + 1;

			return s;
		}

		private static byte[] ParseBinary(string input, out int bytesRead)
		{
			string numString = "";
			int i = 1;
			if (!(new char[] { 'b', 'B' }.Contains(input[0])))
			{
				throw new FormatException("Input doesn't match the format.");
			}

			while (!(new char[] { 'b', 'B' }.Contains(input[i])))
			{
				if (new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' }.Contains(input[i]))
				{
					numString += input[i++];
				}
				else
				{
					throw new FormatException("Input doesn't match the format.");
				}
			}

			i++;

			if (!int.TryParse(numString, out int num))
			{
				throw new FormatException("Input doesn't match the format.");
			}

			bytesRead = i + num;

			return Encoding.ASCII.GetBytes(input.Substring(i, num));
		}

		private static List<object> ParseArray(string input, out int bytesRead)
		{
			if (input[0] != '[')
			{
				bytesRead = 0;
				throw new FormatException("Input doesn't match the format.");
			}

			List<object> arr = new List<object>();
			int i = 1;

			bool next = true;

			while (input[i] != ']')
			{
				if (input[i] == ' ' || input[i] == '\t' || input[i] == '\n')
				{
					i++;
				}
				else if (input[i] == ',')
				{
					next = true;
					i++;
				}
				else
				{
					if (!next)
					{
						throw new FormatException("Input doesn't match the format.");
					}

					arr.Add(ParseValue(input.Substring(i), out int br));
					i += br;
					next = false;
				}
			}

			bytesRead = i + 1;
			return arr;
		}

		private static Dictionary<object, object> ParseObject(string input, out int bytesRead)
		{
			int i = 0;

			Dictionary<object, object> d = new Dictionary<object, object>();

			object key = null;
			object value = null;

			bool isVal = false;
			bool next = true;

			if (input[i++] != '{')
			{
				bytesRead = 0;
				throw new FormatException("Input doesn't match the format.");
			}
			else
			{
				while (input[i] != '}')
				{
					if ((new List<char> { ' ', '\n', '\t' }).Contains(input[i]))//whitespace
					{
						i++;
					}
					else if (input[i] == ',')//next
					{
						next = true;
						isVal = false;
						i++;
					}
					else if (input[i] == ':')//val
					{
						i++;
						if (!next == !isVal)//if already read a key and not a value
						{
							isVal = true;
						}
						else
						{
							throw new Exception("input is not in the right format");
						}
					}
					else//value
					{
						if ((!next && !isVal) || (next && isVal))//if not next set and not a value or if next set and value
						{
							throw new Exception("input is not in the right format");
						}

						object v = ParseValue(input.Substring(i), out int _bytesRead);
						i += _bytesRead;
						next = false;

						if (!isVal)//key
						{
							key = v;
							d[key] = null;
						}
						else if (isVal)//value
						{
							value = v;
							d[key] = value;
						}
					}
				}
			}

			bytesRead = i + 1;
			return d;
		}

		public static object ParseJSON(string json)
		{
			return ParseValue(json, out _);
		}
		#endregion
	}
}
