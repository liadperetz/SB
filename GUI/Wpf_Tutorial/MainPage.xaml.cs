﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;


namespace Wpf_Tutorial
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
        }

        static int SendFile(string path)
        {
            // Create a http request to the server endpoint that will pick up the
            // file and file description.
            HttpWebRequest requestToServerEndpoint = (HttpWebRequest)WebRequest.Create("http://127.0.0.1:8085");
            string boundaryString = "----SomeRandomText";
            string fileUrl = @path;

            MessageBox.Show(fileUrl);

            // Set the http request header \\
            requestToServerEndpoint.Method = WebRequestMethods.Http.Post;
            requestToServerEndpoint.ContentType = "multipart/form-data; boundary=" + boundaryString;
            requestToServerEndpoint.KeepAlive = true;
            requestToServerEndpoint.Credentials = System.Net.CredentialCache.DefaultCredentials;
            
            // Use a MemoryStream to form the post data request,
            // so that we can get the content-length attribute.
            MemoryStream postDataStream = new MemoryStream();
            StreamWriter postDataWriter = new StreamWriter(postDataStream);

            // Include value from the myFileDescription text area in the post data
            /* postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
             postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}",
             "myFileDescription",
             "A sample file description");

             // Include the file in the post data
             postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
             postDataWriter.Write("Content-Disposition: form-data;"
             + "name=\"{0}\";"
             + "filename=\"{1}\""
             + "\r\nContent-Type: {2}\r\n\r\n",
             "myFile",
             Path.GetFileName(fileUrl),
             Path.GetExtension(fileUrl));
             postDataWriter.Write("Data:");*/
            postDataWriter.Flush();



            // Read the file
            FileStream fileStream = new FileStream(fileUrl, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                postDataStream.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();
            postDataWriter.Flush();

            // Set the http request body content length
            requestToServerEndpoint.ContentLength = postDataStream.Length;

            // Dump the post data from the memory stream to the request stream
            using (Stream s = requestToServerEndpoint.GetRequestStream())
            {
                postDataStream.WriteTo(s);
            }
            postDataStream.Close();
            return 1;
        }

        private void Button_Click_Scan_File(object sender, RoutedEventArgs e)
        {
        //תתקן שגאות כתיב
            Process.Start(@"C:\Users\User\Desktop\Sandy\send file  Client Server\HTTPServer\HTTPServer\bin\Debug\HTTPServer.exe");
            MessageBox.Show("The file was transfered succefully");
            //MessageBox.Show("Error! The file was Not transfered succefully");
            int response = SendFile(txtEditor.Text);
            NavigationService.Navigate(new Scan());
        }

        private void Button_Click_last_logs(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new LastLogs());
        }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "exe Files (*.exe)|*.exe|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = @"C:\Users\User\Desktop\";

            if (openFileDialog.ShowDialog() == true)
            {
                txtEditor.Text = openFileDialog.FileName;
                txtEditor.Visibility = Visibility.Visible;
                analysisButton.Visibility = Visibility.Visible;
                selectedFilesLabel.Visibility = Visibility.Visible;
            }
        }

        private void ImagePanel_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                txtEditor.Text = files[0];
                txtEditor.Visibility = Visibility.Visible;
                analysisButton.Visibility = Visibility.Visible;
                selectedFilesLabel.Visibility = Visibility.Visible;
            }
        }
    }
}
