﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Connections;
using System.Diagnostics;



namespace Server
{
    class Program
    {
        //INIT global variables
        private static string _filename = @"C:\Users\User\Desktop\SB\Dest";
        private static string _reportPath = @"C:\Users\User\Desktop\SB\report.txt";

        static void Main(string[] args)
        {
            //INIT
            TcpListener listener = new TcpListener(IPAddress.Any, 8095);
            
            Listen(listener);

            Console.WriteLine("DONE. :)");
            Thread.Sleep(3000);
        }

        /*Input: listener - variable from type TcpListener
         *Ouptut: None
         * Description: This function responsible to the listening. Listen to request from specific port on specific ip
         *              and after it get connection request, it create client and call the clientHandle function.*/
        private static void Listen(TcpListener listener)
        {
            //Start listening
            listener.Start();
            Console.WriteLine("Waiting for a connection...\n");

            //Accept connection
            TcpClient client = listener.AcceptTcpClient();

            listener.Stop();
            //CHECK
            Console.WriteLine("Accepted the connection\n");

            ClientHandle(client);

        }

        /*Input: client - variable from type TcpClient.
         *Output: None
         *Description: Responsible manage the action of the client: send, receive, write, read...
         */
        private static void ClientHandle(TcpClient client)
        {
            //Print basic settings about the connection - Check
            Console.WriteLine("Connected\n");
            Console.WriteLine("ip: {0}\nport: {1}\n", ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString(), (client.Client.RemoteEndPoint as IPEndPoint).Port);

            //Create Connection variable - the protocol ofer built which take care to the application level.
            Connection connect = new Connection(client);

            //Send response message
            ResponseMessage(connect);

            //Read the message fron the client
            Dictionary<object, object> input = (Dictionary<object, object>)connect.Read();

            Console.WriteLine("Got the message");

            ResponseMessage(connect);

            //Create the file path
            _filename = Path.Combine(_filename, ((string)input["filename"]));

            Console.WriteLine("File name: {0}", _filename);
            
            //Write it to a .exe file
            WriteToFile((byte[])input["exeContent"]);

            Console.WriteLine("Wrote to file");


            Console.WriteLine("Opening file");
            ProcessStartInfo pi = new ProcessStartInfo();
            pi.Verb = "runas";
            pi.FileName = _filename;
            Process.Start(pi);

            Console.WriteLine("File opened");

            //Process.Start(_filename);

            //Run the tests
            RunTests();

            writeAndReturnReport(connect, Path.GetFileName(_filename));
        }

        private static void RunTests()
        {
            //Run Connection test
            string c_path = @"C:\Users\User\Desktop\SB\Checks\connection test.exe";
            Process proc = System.Diagnostics.Process.Start(c_path, Path.GetFileName(_filename));

            // Make the program wait until the process will finish its run.
            try
            {
                while (Process.GetProcessById(proc.Id) != null)
                {
                }
            }
            catch (Exception e) { }

            //Run the Duplicate check.
            c_path = @"C:\Users\User\Desktop\SB\Checks\File duplicator Test.exe";
            proc = System.Diagnostics.Process.Start(c_path); //Enter here the parameters you need.

            //Waits for the check to be finished
            try
            {
                while (Process.GetProcessById(proc.Id) != null)
                {
                }
            }
            catch (Exception e) { }
            Console.WriteLine("File Changer test completed");
        }

        private static void writeAndReturnReport(Connection connect, string nameOfFile)
        {
            List<string> pathes = new List<string>();

            //The destination path (output)
            pathes.Add(_reportPath);

            //The inputs
            pathes.Add(@"C:\Users\User\Desktop\SB\reports\temp.txt");
            pathes.Add(@"C:\Users\User\Desktop\SB\reports\fc_Output.txt");

            List<string> lines = new List<string>();

            mergeReports(pathes, nameOfFile);
            ReturnReport(connect);
        }

        static void mergeReports(List<string> pathes, string fileName)
        {
            string[] tempLines;
            List<string> tempList = new List<string>();
            int j = 0, k = 0;

            List<string> output = new List<string>();
            output.Add("File Name: " + fileName);
            output.Add("Date: " + DateTime.Now.Date.ToShortDateString());
            output.Add("Hour: " + DateTime.Now.TimeOfDay.ToString().Remove(8));
            output.Add("Scan Duration: " + "0:01");
            output.Add("Scan Results:");

            for (int i = 1; i < pathes.Count; i++)
            {
                tempLines = System.IO.File.ReadAllLines(pathes[i]);
                tempList = new List<string>(tempLines);

                //Deletes repetitions, removes empty lines
                j = 0;
                while (j < tempList.Count)
                {
                    if(tempList[j] == "")
                    {
                        tempList.RemoveAt(j);
                    }

                    for (k = j + 1; k < tempList.Count; k++)
                    {
                        if(tempList[j] == tempList[k])
                        {
                            tempList.RemoveAt(k);
                        }
                    }

                    j++;
                }

                foreach (string line in tempList)
                {
                    output.Add(line);
                }
            }

            //If all of the reports are empty
            if(output.Count <= 5)
            {
                output.Add("Nothing special has been found during the scan");
            }

            File.WriteAllLines(pathes[0], output);
        }

        private static void ReturnReport(Connection connect)
        {
            SendFile(connect, _reportPath);
        }

        private static void ResponseMessage(Connection connect)
        {
            connect.SendObject("Success");
        }

        /*public static string GetLocalIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }*/

        /*Input: content - the content of the .exe file that the user sent to check
         *Output: None.
         *Description: The function gets the file content from the user and create the same file in the VM.*/
        private static void WriteToFile(byte[] content)
        {
            //If is there already a file with this name in this directory, it will delete it.
            if (File.Exists(_filename))
            {
                File.Delete(_filename);
            }

            //Create empty file.
            FileStream fileStream = File.Create(_filename);
            fileStream.Close();

            //Write the content from the message to the file - the binary part.
            using (FileStream output = new FileStream(_filename, FileMode.Open, FileAccess.Write))
            {
                output.Write(content, 0, content.Length);
                //CHECK
                Console.WriteLine("\nI finished copy\n");
            }
        }

        private static void SendFile(Connection connect, string filename)
        {
            //INIT
            byte[] content = ReadFile(filename);
            Dictionary<object, object> messageContent = new Dictionary<object, object>();

            //Enter values to the dictionary.
            messageContent["filename"] = FindFilename(filename);
            messageContent["Content"] = ReadFile(filename);

            //Send the file
            Console.WriteLine(Connection.JsonToString(messageContent));

            connect.SendObjectBinary(messageContent);

            Console.WriteLine("Sent the file :D");

        }

        private static byte[] ReadFile(string filename)
        {
            //Read the exe file to an array of byte 
            byte[] buffer = File.ReadAllBytes(filename);

            return buffer;
        }

        private static string FindFilename(string filename)
        {
            //string output = 
            return filename.Substring(filename.LastIndexOf('\\') + 1, (filename.Length - filename.LastIndexOf('\\') - 1));
            //output = output.Substring(1, output.Length);
            //return output;
        }
    }
}
