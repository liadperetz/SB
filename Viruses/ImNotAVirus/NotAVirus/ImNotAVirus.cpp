#include <iostream>
#include <chrono>
#include <thread>
using namespace std::chrono_literals;

int main()
{
	std::this_thread::sleep_for(2s);
	std::cout << "Im" << std::endl;
	std::this_thread::sleep_for(2s);
	std::cout << "Not" << std::endl;
	std::this_thread::sleep_for(2s);
	std::cout << "A Virus!" << std::endl;
	std::this_thread::sleep_for(2s);
	return 0;
}

