#include <iostream>
#include <chrono>
#include <thread>
using namespace std::chrono_literals;

int main()
{
	for (size_t i = 0; i < 5; i++)
	{
		std::cout << "1 + 2 is 3" << std::endl;
		std::this_thread::sleep_for(0.5s);
		std::cout << "3 + 4 is 7" << std::endl;
		std::this_thread::sleep_for(0.5s);
		std::cout << "3 + 7 = 10" << std::endl;
		std::this_thread::sleep_for(0.5s);
		std::cout << "10 - 0 = 1 (AmaZinG!)" << std::endl;
		std::this_thread::sleep_for(0.5s);
	}
	
	return 0;
}

