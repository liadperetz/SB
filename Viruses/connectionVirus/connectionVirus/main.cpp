#include <iostream>
#include <string>
#include <chrono>
#include <thread>

int main()
{
	using namespace std::chrono_literals;
	std::cout << "listening on port 443" << std::endl;
	std::this_thread::sleep_for(10s);
	std::cout << "Timeout error - No request have been received" << std::endl;
	std::this_thread::sleep_for(2s);
	return 0;
}