﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Merge_Reports
{
    class Program
    {
        //Very IMPORTANT!
        //Send the file name as an argument!       !!               !!          !!
        static void Main(string[] args)
        {
            List<string> list = new List<string>();

            //The destination path (output)
            list.Add(@"C:\Users\User\Desktop\SB\Merge Reports\Merge Reports\bin\Debug\Text444 .txt");

            //The inputs
            list.Add(@"C:\Users\User\Desktop\SB\Merge Reports\Merge Reports\bin\Debug\Text1.txt");
            list.Add(@"C:\Users\User\Desktop\SB\Merge Reports\Merge Reports\bin\Debug\Text2.txt");

            string fileName = (args.Length > 0) ? args[0] : "(File Name)";
            mergeReports(list, fileName);
        }

        static void mergeReports(List<string> pathes, string fileName)
        {
            string[] tempLines;

            List<string> output = new List<string>();
            output.Add("File Name: " + fileName);
            output.Add("Date: " + DateTime.Now.Date.ToShortDateString());
            output.Add("Hour: " + DateTime.Now.TimeOfDay.ToString().Remove(8));
            output.Add("Scan Duration: " + "0:01");
            output.Add("Scan Results:");

            for (int i = 1; i < pathes.Count; i++)
            {
                tempLines = System.IO.File.ReadAllLines(pathes[i]);

                foreach (string line in tempLines)
                {
                    output.Add(line);
                }
            }
            
            File.WriteAllLines(pathes[0], output);
        }
    }
}
