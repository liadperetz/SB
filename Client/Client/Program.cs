﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Connections;

namespace Client
{
    class Program
    {
        //INIT Global variables
        private static string _filename = @"C:\Users\User\Desktop\SB\check.exe";
        private static string _filepath = @"C:\Users\User\Desktop\SB\reports\";

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                _filename = args[0];
            }


            //Create TcpClient for create Connection variable.
            IPEndPoint serverIpEndPoint = new IPEndPoint(GetIpAddress(), 8095);
            TcpClient server = new TcpClient();
            server.Connect(serverIpEndPoint);

            //Craete Connection variable
            Connection serverConnection = new Connection(server);

            Console.WriteLine("Created connection\n"); 

            if(GetResponse(serverConnection) == true)
            {
                SendFile(serverConnection);
            }

            //Get the response from the server - The file transfered well or not
            string response = (string)serverConnection.Read();
            Console.WriteLine("Transformation: {0}", response);

            //Receive back the report file
            Console.WriteLine("Waiting for the report...");

            GetReport(serverConnection);

        }


        private static void ResponseMessage(Connection connect)
        {
            connect.SendObject("Success");
        }



        /*Input: content - the content of the .exe file that the user sent to check
         *Output: None.
         *Description: The function gets the file content from the user and create the same file in the VM.*/
        private static void WriteToFile(byte[] content, string filename)
        {
            //If is there already a file with this name in this directory, it will delete it.
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }

            /*using (FileStream file = File.Create(_filename)) //_filename = Path + filename.
            {
                file.Write(content, 0, content.Length);
            }*/

            FileStream fileStream = File.Create(filename);
            fileStream.Close();

            using (FileStream output = new FileStream(filename, FileMode.Open, FileAccess.Write))
            {
                output.Write(content, 0, content.Length);
                //CHECK
                Console.WriteLine("\nI finished copy\n");
            }
        }

        private static void SendFile(Connection serverConnection)
        {
            //INIT
            byte[] content = ReadFile();
            Dictionary<object, object> messageContent = new Dictionary<object, object>();

            //Enter values to the dictionary.
            messageContent["filename"] = FindFilename();
            messageContent["exeContent"] = ReadFile();

            //Send the file
            Console.WriteLine(Connection.JsonToString(messageContent));

            serverConnection.SendObjectBinary(messageContent);

            Console.WriteLine("Sent the file :D");

        }


        /*Input: None
        *Output: ip - String include the IP address the user entered.
        *Description: The function receive Ip address from the user, checks if it legall and then return it.*/
        private static byte[] ReadFile()
        {
            //Read the exe file to an array of byte 
            byte[] buffer = File.ReadAllBytes(_filename);

            return buffer;
        }




        private static bool GetResponse(Connection connect)
        {
            if((string)connect.Read() == "Success")
            {
                return true;
            }
            return false;
        }







        /*Input: None
         *Output: ip - String include the IP address the user entered.
         *Description: The function receive Ip address from the user, checks if it legall and then return it.*/
        private static IPAddress GetIpAddress()
        {
            //INIT
            string ips = null;
            IPAddress ip = null;
            bool isValid = false;
            //Run until the user enter legal IP address
            while (!isValid)
            {
                //Gets the IP address from the user.
                Console.WriteLine("Enter the IP address of the server: ");
                ips = Console.ReadLine();
                //Check if the IP address is legal.
                isValid = IPAddress.TryParse(ips, out ip);
            }

            //CHECK
            Console.WriteLine("IP: {0}", ip);

            return ip;
        }

        private static string FindFilename()
        {
            //string output = 
            return _filename.Substring(_filename.LastIndexOf('\\') + 1, (_filename.Length - _filename.LastIndexOf('\\') - 1));
            //output = output.Substring(1, output.Length);
            //return output;
        }

        private static void GetReport(Connection serverConnection)
        {
            string filename = "";
            Dictionary<object, object> input = (Dictionary<object, object>)serverConnection.Read();

            Console.WriteLine("Got the message");

            //serverConnection.SendObject("Success");

            //Create the file path
            filename = Path.Combine(_filepath, ((string)input["filename"]));

            Console.WriteLine("File name: {0}", filename);

            //Write it to a .exe file
            WriteToFile((byte[])input["Content"], filename);

            Console.WriteLine("Wrote to file");

        }




    }
}
