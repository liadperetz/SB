#include <iostream>
#include <string>
#include <fstream>

void listenNetwork();
std::string findProcessID(std::string processName);
std::string getPIDFromLine(std::string line);
void createReport(std::string pid);
//void deleteDuplicate();

std::string _filename;

int main(int argc, char* argv[])
{
	_filename = argv[1];
	listenNetwork();
	return 0;
}


void listenNetwork()
{
	//INIT

	std::string pid;

	//Find the PID of the process.
	pid = findProcessID(_filename);
	
	if (pid != "")
	{
		createReport(pid);
	}


	
}

/*Input: pid - String include the Process ID of the program we want to check 
 *Output: None
 *Description: The function check in the netstat if the program is trying to create connection on Internet, and 
 *             write the details in the report file. 
*/
void createReport(std::string pid)
{
	//INIT
	FILE   *pPipe;
	char temp[5000]; //Using char array becuase the functions I'm using here are C functions.
	std::string buffer;
	std::string command = "netstat -a -n -o | findstr \"";

	
	std::ofstream reportFile("C:\\Users\\User\\Desktop\\SB\\reports\\temp.txt");
	



	command += (pid + "\"");

	if (reportFile.is_open())
	{
		while (findProcessID(_filename).compare(pid) == 0)
		{
			if ((pPipe = _popen(command.c_str(), "rt")) != NULL)
			{
				while (fgets(temp, 5000, pPipe))
				{
					buffer = temp;
					reportFile << buffer << std::endl;
				}

			}
		}
		reportFile.close();
	}

}

std::string findProcessID(std::string processName)
{
	//INIT
	FILE   *pPipe;
	char temp[5000]; //Using here char array becuase the functions I'm using here are C functions.
	std::string buffer;
	std::string line;
	std::string command = "tasklist | findstr \"";
	command += _filename;
	command += "\"";



	if ((pPipe = _popen(command.c_str(), "rt")) == NULL)
	{
		exit(1);
	}

	//Check - print the output of the _popen command
	while (fgets(temp, 5000, pPipe))
	{
	}

	//save the content of the char array in string type variable.
	buffer = temp;

	/*buffer.find_first_of('\n') != std::string::npos = run untill the end of the string
	* line.substr(0, line.find_first_of(' ')).compare(processName) = run until its find its name.
	* buffer.length() != 0 = run until the end of the file.
	*/
	while ((buffer.find_first_of('\n') != std::string::npos) && line.substr(0, line.find_first_of(' ')).compare(processName) != 0 && buffer.length() != 0)
	{
		line = buffer.substr(0, buffer.find_first_of('\n'));
		buffer = buffer.substr((buffer.find_first_of('\n') + 1), buffer.length() - buffer.find_first_of('\n'));
	}

	//If the process exist in the task manager, call the function that will find the PID
	if (line.substr(0, line.find_first_of(' ')).compare(processName) == 0)
	{
		return getPIDFromLine(line);
	}
	else
	{
		return "";
	}
}

std::string getPIDFromLine(std::string line)
{
	//INIT
	std::string pid = "";

	//Find the PID in the line and save it in the variable PID
	pid = line.substr(line.find_first_of(' '), (line.length() - line.find_first_of(' ') - 1));
	pid = pid.substr(pid.find_first_not_of(' '), pid.size() - pid.find_first_not_of(' '));
	pid = pid.substr(0, pid.find_first_of(' '));

	return pid; 
}


/*void deleteDuplicate()
{
	//INIT
	std::ifstream infile;
	std::ofstream outfile;
	std::string line, temp;
	
	infile.open("C:\\Users\\User\\Desktop\\SB\\reports\\temp.txt");
	outfile.open("C:\\Users\\User\\Desktop\\SB\\reports\\connection_report.txt");

	if (infile.is_open() && outfile.is_open())
	{
		std::getline(infile, line);
		for (temp; std::getline(infile, temp);)
		{
			if (temp.compare("\n") != 0)
			{

			}
		}
		infile.close();
		outfile.close();
	}
	



}*/