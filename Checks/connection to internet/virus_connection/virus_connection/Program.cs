﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;

namespace virus_connection
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener listener = new TcpListener(IPAddress.Any, 8095);

            Listen(listener);
            
        }

        /*Input: listener - variable from type TcpListener
         *Ouptut: None
         * Description: This function responsible to the listening. Listen to request from specific port on specific ip
         *              and after it get connection request, it create client and call the clientHandle function.*/
        private static void Listen(TcpListener listener)
        {
            //Start listening
            listener.Start();
            Console.WriteLine("Waiting for a connection...\n");

            var timerTimer = new Timer((state) => { listener.Stop(); }, null, 60000, Timeout.Infinite);

            try
            {
                //Accept connection
                TcpClient client = listener.AcceptTcpClient();
            }
            catch(Exception e)
            {

            }

            listener.Stop();

        }
    }
}
