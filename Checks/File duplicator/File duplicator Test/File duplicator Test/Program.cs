﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Collections.Generic;
using System.Collections;
using System.Security.Cryptography;

namespace File_duplicator_Test
{
    class Program
    {
        public static void Main(string[] args)
        {
            string currPath = Directory.GetCurrentDirectory();
            string path = @"c:\Program Files (x86)";
            //string path = @"C:\Users\User\OneDrive\מגשימים";

            string dataBeforePath = currPath + @"\All_Files_Before.txt";
            File.WriteAllText(dataBeforePath, "");
            StreamWriter dataFileBefore = new System.IO.StreamWriter(dataBeforePath, true);
            
            listFiles(path, dataFileBefore);
            dataFileBefore.Close();

            Console.WriteLine("Before file ran");
            Console.ReadKey();


            string dataAfterPath = currPath + @"\All_Files_After.txt";
            File.WriteAllText(dataAfterPath, "");
            StreamWriter dataFileAfter = new System.IO.StreamWriter(dataAfterPath, true);
            listFiles(path, dataFileAfter);
            dataFileAfter.Close();

            Console.WriteLine("After file ran");
            Console.ReadKey();
            
            string outputPath = currPath + @"\data_Duplicator_Output.txt";
            compareDataFiles(dataBeforePath, dataAfterPath, outputPath);
        }
        
        public static void compareDataFiles(string dataBeforePath, string dataAfterPath, string outputPath)
        {
            //Clears the output file
            File.WriteAllText(outputPath, "");

            string[] linesBefore = System.IO.File.ReadAllLines(dataBeforePath);
            string[] linesAfter = System.IO.File.ReadAllLines(dataAfterPath);

            //Creates a list of all of the lines
            HashSet<string> linesBeforeList = new HashSet<string>(linesBefore);
            HashSet<string> linesAfterList = new HashSet<string>(linesAfter);
            HashSet<string> outputList = new HashSet<string>();

            //Remembers the name of the last file
            string lastFileName = "";

            foreach (string line in linesBeforeList)
            {
                if(line[0] == '\'')
                {
                    lastFileName = line;
                }

                if(linesAfterList.Contains(line) == false)
                {
                    //If its a file
                    if(line[0] == '\'')
                    {
                        outputList.Add("File " + line + " Was deleted");
                    }
                    //If its an MD5
                    else
                    {
                        outputList.Add("File " + lastFileName + " Was changed");
                    }
                }
            }

            foreach (string line in linesAfterList)
            {
                if (linesBeforeList.Contains(line) == false)
                {
                    if (line[0] == '\'')
                    {
                        outputList.Add("File " + line + " Was Created");
                    }
                }
            }

            File.WriteAllLines(outputPath, outputList);
        }

        public static void listFiles(string path, StreamWriter dataFile)
        {
            
            if (File.Exists(path))
            {
                // This path is a file
                ProcessFile(path, dataFile);
            }
            else if (Directory.Exists(path))
            {
                // This path is a directory
                ProcessDirectory(path, dataFile);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }
        }

        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public static void ProcessDirectory(string targetDirectory, StreamWriter dataFile)
        {
            
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);

            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName, dataFile);
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)
            {
                try
                {
                   ProcessDirectory(subdirectory, dataFile);
                }
                catch (Exception e) { }
            }
            
        }

        // Insert logic for processing found files here.
        public static void ProcessFile(string path, StreamWriter dataFile)
        {
            dataFile.WriteLine("'{0}'.", path);
            dataFile.WriteLine(CalculateMD5(path));
        }

        static string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    //Creats the string representation for the md5 of the file
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }
    }
}
