﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateDictionary
{
    class GenerateDictionary
    {
        static Dictionary<string, string> dicBefore = new Dictionary<string, string>();
        static int a = 0, b = 0;

        static void Main(string[] args)
        {
            /*
                The program geneerates a file containing the names of the files followed by thier MD5
            */
            System.IO.File.WriteAllText(@"C:\Users\User\Desktop\SB\reports\SavedDic.txt", string.Empty);
            TextWriter tw = new StreamWriter(@"C:\Users\User\Desktop\SB\reports\SavedDic.txt");
            
            string currPath = Directory.GetCurrentDirectory();
            string path = @"c:\Program Files (x86)";
            
            listFiles(path, tw);
            Console.WriteLine("a: " + a + "  b: " + b + "A+B: " + (a + b).ToString());
            tw.Dispose();
            string[] lines = File.ReadAllLines(@"C:\Users\User\Desktop\SB\reports\SavedDic.txt");
            Console.WriteLine("Num of lines: " + lines.Length);
            Console.WriteLine(lines[lines.Length - 1]);
            Console.ReadKey();
        }

        public static void listFiles(string path, TextWriter tw)
        {
            if (File.Exists(path))
            {
                // This path is a file
                ProcessFile(path, tw);
            }

            else if (Directory.Exists(path))
            {
                // This path is a directory
                ProcessDirectory(path, tw);
            }

            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }
        }

        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public static void ProcessDirectory(string targetDirectory, TextWriter tw)
        {

            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);

            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName, tw);
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)
            {
                try
                {
                    ProcessDirectory(subdirectory, tw);
                }
                catch (Exception e) { }
            }

        }
        
        public static void ProcessFile(string path, TextWriter tw)
        {
            //The weather changes too frequently lol
            if (!path.Contains(@"c:\Program Files (x86)\Windows Sidebar\Gadgets\Weather.Gadget"))
            {
                a++;
                tw.WriteLine( "{" + path + "}" );
                tw.WriteLine( CalculateMD5(path) );
            }
        }

        static string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    b++;
                    //Creats the string representation for the md5 of the file
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }
    }
}
