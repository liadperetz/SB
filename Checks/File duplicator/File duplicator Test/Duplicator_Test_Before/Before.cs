﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;

namespace Duplicator_Test_Before
{
    class Program
    {
        public static void Main(string[] args)
        {
            string currPath = Directory.GetCurrentDirectory();
            string path = @"C:\Users\User\OneDrive\מגשימים";

            string dataBeforePath = currPath + @"\All_Files_Before.txt";
            File.WriteAllText(dataBeforePath, "");
            StreamWriter dataFileBefore = new System.IO.StreamWriter(dataBeforePath, true);

            listFiles(path, dataFileBefore);
            dataFileBefore.Close();
        }

        public static void listFiles(string path, StreamWriter dataFile)
        {

            if (File.Exists(path))
            {
                // This path is a file
                ProcessFile(path, dataFile);
            }
            else if (Directory.Exists(path))
            {
                // This path is a directory
                ProcessDirectory(path, dataFile);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }
        }

        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public static void ProcessDirectory(string targetDirectory, StreamWriter dataFile)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);

            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName, dataFile);
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)
            {
                ProcessDirectory(subdirectory, dataFile);
            }
        }

        // Insert logic for processing found files here.
        public static void ProcessFile(string path, StreamWriter dataFile)
        {
            dataFile.WriteLine("'{0}'.", path);
        }

    }
}
