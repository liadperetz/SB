﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Collections.Generic;
using System.Collections;
using System.Security.Cryptography;

namespace File_duplicator_Test
{
    class Program
    {
        //Key- file name (path)
        //Value- MD5 of the file
        static Dictionary<string, string> dicBefore = new Dictionary<string, string>();
        static Dictionary<string, string> dicAfter = new Dictionary<string, string>();

        public static void Main(string[] args)
        {
            //INIT
            string currPath = Directory.GetCurrentDirectory();
            string path = @"c:\Program Files (x86)";
            string dataBeforePath = @"C:\Users\User\Desktop\SB\reports\SavedDic.txt";

            //Reads the dic from before the scan
            readSavedDic(dataBeforePath);

            Console.WriteLine("Saved Dic successfuly added");
            Console.WriteLine("Continues...");

            string dataAfterPath = currPath + @"C:\Users\User\Desktop\SB\reports\All_Files_After.txt";
            
            //Writes the dic after the scan
            listFiles(path);

            Console.WriteLine("Generetes output...");
            
            //Compares between the dics
            string outputPath = @"C:\Users\User\Desktop\SB\reports\fc_Output.txt";
            compareDataFiles(outputPath);
        }
        
        public static void compareDataFiles(string outputPath)
        {
            //Clears the output file
            File.WriteAllText(outputPath, "");

            List<string> outputList = new List<string>();

            //Scans the dictionary from before the scan, and compars it with the dic after
            foreach (KeyValuePair<string, string> pair in dicBefore)
            {
                string path = pair.Key;
                if ((path.Contains(@"c:\Program Files (x86)\Google\Chrome\Application\chrome.exe") || (!path.Contains(@"c:\Program Files (x86)\Windows Sidebar\Gadgets\Weather.Gadget") && !(path.Contains("Google")))))
                {
                    //If a file was deleted
                    if (dicAfter.ContainsKey(pair.Key) == false)
                    {
                        outputList.Add("File " + pair.Key + " Was deleted");
                    }
                    //If a file was changed
                    else if (dicAfter.ContainsValue(pair.Value) == false)
                    {
                        outputList.Add("File " + pair.Key + " Was changed");
                    }
                }
                /*
                //If a file was deleted
                if (dicAfter.ContainsKey(pair.Key) == false)
                {
                    outputList.Add("File " + pair.Key + " Was deleted");
                }
                //If a file was changed
                else if (dicAfter.ContainsValue(pair.Value) == false)
                {
                    outputList.Add("File " + pair.Key + " Was changed");
                }*/
            }

            //Now compares the dic after to the dic before
            foreach (KeyValuePair<string, string> pair in dicAfter)
            {
                string path = pair.Key;
                if ((path.Contains(@"c:\Program Files (x86)\Google\Chrome\Application\chrome.exe") || path.Contains(@"c:\Program Files (x86)\Google\Chrome\Application\click_Here_To_Fix_Chrome.exe") || (!path.Contains(@"c:\Program Files (x86)\Windows Sidebar\Gadgets\Weather.Gadget") && !(path.Contains("Google")))))
                {
                    //If a file was created
                    if (dicBefore.ContainsKey(pair.Key) == false)
                    {
                        outputList.Add("File " + pair.Key + " Was created");
                    }
                }
                /*
                //If a file was created
                if (dicBefore.ContainsKey(pair.Key) == false)
                {
                    outputList.Add("File " + pair.Key + " Was created");
                }*/
            }

            //Writes the output of the check
            File.WriteAllLines(outputPath, outputList);
        }

        public static void listFiles(string path)
        {
            
            if (File.Exists(path))
            {
                // This path is a file
                ProcessFile(path);
            }
            else if (Directory.Exists(path))
            {
                // This path is a directory
                ProcessDirectory(path);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }
        }

        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain
        public static void ProcessDirectory(string targetDirectory)
        {
            
            // Process the list of files found in the directory
            string[] fileEntries = Directory.GetFiles(targetDirectory);

            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName);
            }

            // Recurse into subdirectories of this directory
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)
            {
                try
                {
                   ProcessDirectory(subdirectory);
                }
                catch (Exception e) { }
            }
            
        }

        //Adds each file to the dictionary
        public static void ProcessFile(string path)
        {
            //The weather changes too frequently lol
            if (!path.Contains(@"c:\Program Files (x86)\Windows Sidebar\Gadgets\Weather.Gadget"))
            {
                dicAfter.Add("{" + path + "}", CalculateMD5(path));
            }
        }

        //Calculates and returns the MD5 of the given path
        static string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    //Creats the string representation for the md5 of the file
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        //Enters the dictionary which is saved in the file into the dictionary in the RAM
        //(Into the dictionary in the proccess)
        public static void readSavedDic(string path)
        {
            string[] linesArray = File.ReadAllLines(path);

            if (linesArray.Length % 2 != 0)
            {
                Console.WriteLine("Odd number of lines!!! ERRRORRRR!");
            }

            else
            {
                for (int i = 0; i < linesArray.Length; i += 2)
                {
                    dicBefore.Add(linesArray[i], linesArray[i + 1]);
                }
            }
        }
    }
}
