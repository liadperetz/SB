﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Collections.Generic;
using System.Collections;
using System.Security.Cryptography;

namespace File_duplicator_Test
{
    class Program
    {
        static Dictionary<string, string> dicBefore = new Dictionary<string, string>();
        static Dictionary<string, string> dicAfter = new Dictionary<string, string>();

        public static void Main(string[] args)
        {
            string currPath = Directory.GetCurrentDirectory();
            string path = @"c:\Program Files (x86)";
            //string path = @"C:\Users\User\OneDrive\מגשימים";

            string dataBeforePath = currPath + @"\All_Files_Before.txt";
            
            listFiles(path, false);
            
            Console.WriteLine("Before file ran");
            Console.ReadKey();
            Console.WriteLine("Continues...");

            string dataAfterPath = currPath + @"\All_Files_After.txt";
            
            listFiles(path, true);

            Console.WriteLine("Generetes output...");
            
            string outputPath = currPath + @"\data_Duplicator_Output.txt";
            compareDataFiles(outputPath);
        }
        
        public static void compareDataFiles(string outputPath)
        {
            //Clears the output file
            File.WriteAllText(outputPath, "");

            List<string> outputList = new List<string>();
            int i = 0;
            foreach (KeyValuePair<string, string> pair in dicBefore)
            {
                i++;
                if(dicAfter.ContainsKey(pair.Key) == false)
                {
                    outputList.Add("File " + pair.Key + " Was deleted");
                }
                else if (dicAfter.ContainsValue(pair.Value) == false)
                {
                    outputList.Add("File " + pair.Key + " Was changed");
                }
            }
            Console.WriteLine(i);
            foreach (KeyValuePair<string, string> pair in dicAfter)
            {
                if (dicBefore.ContainsKey(pair.Key) == false)
                {
                    outputList.Add("File " + pair.Key + " Was created");
                }
            }

            File.WriteAllLines(outputPath, outputList);
        }

        public static void listFiles(string path, bool IsAfter)
        {
            
            if (File.Exists(path))
            {
                // This path is a file
                ProcessFile(path, IsAfter);
            }
            else if (Directory.Exists(path))
            {
                // This path is a directory
                ProcessDirectory(path, IsAfter);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }
        }

        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public static void ProcessDirectory(string targetDirectory, bool IsAfter)
        {
            
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);

            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName, IsAfter);
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)
            {
                try
                {
                   ProcessDirectory(subdirectory, IsAfter);
                }
                catch (Exception e) { }
            }
            
        }

        // Insert logic for processing found files here.
        public static void ProcessFile(string path, bool IsAfter)
        {
            if(IsAfter)
            {
                dicAfter.Add("{" + path + "}", CalculateMD5(path));
            }

            else
            {
                dicBefore.Add("{" + path + "}", CalculateMD5(path));
            }
        }

        static string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    //Creats the string representation for the md5 of the file
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }
    }
}
