﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;

namespace Duplicator_Test_After
{
    class Program
    {
        public static void Main(string[] args)
        {
            string currPath = Directory.GetCurrentDirectory();
            string path = @"C:\Users\User\OneDrive\מגשימים";

            string dataBeforePath = currPath + @"\All_Files_Before.txt";
            
            string dataAfterPath = currPath + @"\All_Files_After.txt";
            File.WriteAllText(dataAfterPath, "");
            StreamWriter dataFileAfter = new System.IO.StreamWriter(dataAfterPath, true);
            listFiles(path, dataFileAfter);
            dataFileAfter.Close();
            
            string outputPath = currPath + @"\data_Duplicator_Output.txt";
            compareDataFiles(dataBeforePath, dataAfterPath, outputPath);
        }

        public static void compareDataFiles(string dataBeforePath, string dataAfterPath, string outputPath)
        {
            //Clears the output file
            File.WriteAllText(outputPath, "");

            string[] linesBefore = System.IO.File.ReadAllLines(dataBeforePath);
            string[] linesAfter = System.IO.File.ReadAllLines(dataAfterPath);

            //Creates a list of all of the lines
            List<string> linesBeforeList = new List<string>(linesBefore);
            List<string> linesAfterList = new List<string>(linesAfter);
            List<string> outputList = new List<string>();

            foreach (string line in linesBeforeList)
            {
                if (linesAfterList.Contains(line) == false)
                {
                    outputList.Add("File " + line + " Was deleted");
                }
            }

            foreach (string line in linesAfterList)
            {
                if (linesBeforeList.Contains(line) == false)
                {
                    outputList.Add("File " + line + " Was Created");
                }
            }

            File.WriteAllLines(outputPath, outputList);
        }

        public static void listFiles(string path, StreamWriter dataFile)
        {

            if (File.Exists(path))
            {
                // This path is a file
                ProcessFile(path, dataFile);
            }
            else if (Directory.Exists(path))
            {
                // This path is a directory
                ProcessDirectory(path, dataFile);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }
        }

        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public static void ProcessDirectory(string targetDirectory, StreamWriter dataFile)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);

            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName, dataFile);
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)
            {
                ProcessDirectory(subdirectory, dataFile);
            }
        }

        // Insert logic for processing found files here.
        public static void ProcessFile(string path, StreamWriter dataFile)
        {
            dataFile.WriteLine("'{0}'.", path);
        }

    }
}
